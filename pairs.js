const pairs = (obj) => {
    if (!obj || typeof obj !== 'object') return [];
    const newObject = [];
    for (let key in obj){
        newObject.push([key,obj[key]]);
    }
    return newObject;
};

module.exports = pairs;