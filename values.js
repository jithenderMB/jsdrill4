const values = (obj) => {
    if (!obj || typeof obj !== 'object') return {};
    const newArray = [];
    for (let i in obj){
        newArray.push(obj[i]);
    }
    return newArray;
};
module.exports = values;