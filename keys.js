const keys = (obj) => {
    if (!obj || typeof obj !== 'object') return {};
    const newArray = [];
    for (let i in obj){
        newArray.push(i);
    }
    return newArray;
};
module.exports = keys;