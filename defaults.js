function defaults(obj, defaultProps) {
    if (!obj || typeof obj!=='object') return {};
    if(!defaultProps || typeof defaultProps!=='object') return obj;
    for (let key in defaultProps){
        if (!obj[key]) {
            obj[key] = defaultProps[key];
        }
    }
    return obj;
}
module.exports = defaults;
