function mapObject(obj, cb) {
    if (!obj || typeof obj !== 'object') {return {}};
    const newObject = {};
    for (let i in obj){
        newObject[i] = cb(obj[i],i);
    }
    return newObject;
}

module.exports = mapObject;
