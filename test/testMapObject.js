const mapObject = require("../mapObject")

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const result = mapObject(testObject, (val, key) => {
  return val + '-HYD';
});
console.log(result);
