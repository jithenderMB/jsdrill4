function invert(obj) {
    if (!obj || typeof obj !== 'object') return [];
    const newObject = {};
    for (let key in obj){
        newObject[JSON.stringify(obj[key])] = key;
    }
    return newObject;
}

module.exports = invert;